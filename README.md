# interview-97

Follow the instructions inside the readme file

## Getting Started

This is the instructions for Talasho Aug 2018 recruitment program, follow them carefully

### Requirements

To create your project, you need to create a new private repository on [GitLab](https://gitlab.com)

Then you need to create a "Develop" branch from your "Master" branch

Your project has to be developed in "Develop" branch

Finally merge "Develop" into "Master" (DO NOT REMOVE DEVELOP BRANCH AFTERWARDS)

When done, add @Talasho as the "Maintainer" to the repository

### Android & IOS Developers

If you have applied for Android/IOS developer position, these instructions are for you:

you need to create a mobile app with following specs:

* This app will connect to the given API, receive the data and display it
* All the API calls must be asyncronous and run in the background
* All the media(like profile picture) must load asyncronously and be cached properly

#### API Calls

* Get the list of all users (1)

```
https://reqres.in/api/users?page=1
```

* Get user by ID (2)

```
https://reqres.in/api/users/1
```

In a nutshell, create an app that shows a list of users with pagination (call API 1), and user info is shown on touch (call API 2)

### Web Developers

If you have applied for web developer position, these instructions are for you:

you need to create a webiste with following specs:

* This website will have a RESTful API
* This API sends/receives data to/from a database of your choice
* This website must be built with a web framework (Laravel, expressjs, django, ...)
* All the API calls must be asyncronous and run in the background
* All the media(like profile picture) must load asyncronously and be cached properly
* Passwords must be encrypted properly

#### API Calls

* Create an API that gets a list of users (1)
* Create an API that gets a user's info (2)
* Create an API that registers a user (3)

#### Models

* User model : first name, last name, address, email, username, image and password

#### User Interface 

* Register/Login Page
* A page that shows the list of users
* A page that shows a single user

In a nutshell, create website with RESTful APIs that shows a list of users with pagination (call API 1), user info is shown on click (call API 2), 
and user is registered (call API 3)

#### Your optimizations, commits, coding convention and speed will be compared with the other applicants, so do your BEST!
#### Good Luck ;)